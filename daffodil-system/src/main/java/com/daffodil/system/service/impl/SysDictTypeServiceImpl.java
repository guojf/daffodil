package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.constant.Constants;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.entity.Ztree;
import com.daffodil.core.exception.BaseException;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.system.entity.SysDictData;
import com.daffodil.system.entity.SysDictType;
import com.daffodil.system.service.ISysDictTypeService;
import com.daffodil.util.HqlUtils;
import com.daffodil.util.StringUtils;

/**
 * 字典 业务层实现
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysDictTypeServiceImpl implements ISysDictTypeService {

	@Autowired
	private JpaDao jpaDao;

	@Override
	public List<SysDictType> selectDictTypeList(Query<SysDictType> query) {
		List<Object> paras = new ArrayList<Object>();
		StringBuffer hql = new StringBuffer("from SysDictType where 1=1 ");
		HqlUtils.createHql(hql, paras, query);
		return jpaDao.search(hql.toString(), paras, SysDictType.class, query.getPage());
	}
	
	@Override
	public SysDictType selectDictTypeById(String dictTypeId) {
		return jpaDao.find(SysDictType.class, dictTypeId);
	}

	public SysDictType selectDictTypeByType(String dictType) {
		return jpaDao.find("from SysDictType where dictType = ?", dictType, SysDictType.class);
	}

	@Override
	@Transactional
	public void deleteDictTypeByIds(String[] ids) {
		for (String dictTypeId : ids) {
			int count = jpaDao.count("from SysDictData where dictType = ?", dictTypeId);
			if (count > 0) {
				SysDictType sysDictType = this.selectDictTypeById(dictTypeId);
				throw new BaseException("删除字典【" + sysDictType.getDictName() +"】失败，字典已分配，不能删除");
			}
		}
		jpaDao.delete(SysDictType.class, ids);
	}

	@Override
	@Transactional
	public void insertDictType(SysDictType dictType) {
		if (this.checkDictTypeUnique(dictType)) {
			throw new BaseException("新增字典【" + dictType.getDictName() + "】失败，字典类型已存在");
		}
		dictType.setCreateBy(ShiroUtils.getLoginName());
		dictType.setCreateTime(new Date());
		jpaDao.save(dictType);
	}

	@Override
	@Transactional
	public void updateDictType(SysDictType dictType) {
		if (this.checkDictTypeUnique(dictType)) {
			throw new BaseException("修改字典【" + dictType.getDictName() + "】失败，字典类型已存在");
		}
		SysDictType oldDictType = jpaDao.find(SysDictType.class, dictType.getId());
		SysDictData sysDictData = jpaDao.find("from SysDictData where dictType = ?", dictType.getDictType(), SysDictData.class);
		if(null != sysDictData){
			sysDictData.setDictType(oldDictType.getDictType());
			jpaDao.update(sysDictData);
		}
		dictType.setUpdateBy(ShiroUtils.getLoginName());
		dictType.setUpdateTime(new Date());
		jpaDao.update(dictType);
	}

	@Override
	public boolean checkDictTypeUnique(SysDictType dictType) {
		List<Object> paras = new ArrayList<Object>();
		String hql = "from SysDictType where dictType = ? ";
		paras.add(dictType.getDictType());
		if(StringUtils.isNotEmpty(dictType.getId())){
			hql += "and id != ?";
			paras.add(dictType.getId());
		}
		SysDictType sysDictType = jpaDao.find(hql, paras, SysDictType.class);
		if (null != sysDictType) {
			return Constants.NOT_UNIQUE;
		}
		return Constants.IS_UNIQUE;
	}

	public List<Ztree> selectDictTree(SysDictType dictType) {
		List<Ztree> ztrees = new ArrayList<Ztree>();
		List<SysDictType> dictList = jpaDao.search("from SysDictType where dictType = ?", dictType.getDictType(), SysDictType.class);
		for (SysDictType dict : dictList) {
			if (Constants.NORMAL.equals(dict.getStatus())) {
				Ztree ztree = new Ztree();
				ztree.setId(dict.getId());
				ztree.setName(transDictName(dict));
				ztree.setTitle(dict.getDictType());
				ztrees.add(ztree);
			}
		}
		return ztrees;
	}

	public String transDictName(SysDictType dictType) {
		StringBuffer sb = new StringBuffer();
		sb.append("(" + dictType.getDictName() + ")");
		sb.append("&nbsp;&nbsp;&nbsp;" + dictType.getDictType());
		return sb.toString();
	}
}
