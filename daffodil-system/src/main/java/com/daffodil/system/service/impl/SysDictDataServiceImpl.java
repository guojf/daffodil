package com.daffodil.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.system.entity.SysDictData;
import com.daffodil.system.service.ISysDictDataService;
import com.daffodil.util.HqlUtils;

/**
 * 字典 业务层实现
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Service
public class SysDictDataServiceImpl implements ISysDictDataService {
	
	@Autowired
	private JpaDao jpaDao;
	
	@Override
	public List<SysDictData> selectDictDataList(Query<SysDictData> query) {
		List<Object> paras = new ArrayList<Object>();
		StringBuffer hql = new StringBuffer("from SysDictData where 1=1 ");
		HqlUtils.createHql(hql, paras, query);
		return jpaDao.search(hql.toString(), paras, SysDictData.class, query.getPage());
	}
	
	@Override
	public List<SysDictData> selectDictDataByType(String dictType) {
		return jpaDao.search("from SysDictData where status ='0' and dictType = ? order by dictSort asc", dictType, SysDictData.class);
	}

	@Override
	public String selectDictLabel(String dictType, String dictValue) {
		List<Object> paras = new ArrayList<Object>();
		paras.add(dictType);
		paras.add(dictValue);
		SysDictData dictData = jpaDao.find("from SysDictData where status ='0' and dictType = ? and dictValue = ?", paras, SysDictData.class);
		if(null != dictData){
			return dictData.getDictLabel();
		}else{
			return null;
		}
	}

	@Override
	public SysDictData selectDictDataById(String dictId) {
		return jpaDao.find(SysDictData.class, dictId);
	}

	@Override
	@Transactional
	public void deleteDictDataByIds(String[] ids) {
		jpaDao.delete(SysDictData.class, ids);
	}

	@Override
	@Transactional
	public void insertDictData(SysDictData dictData) {
		dictData.setCreateBy(ShiroUtils.getLoginName());
		dictData.setCreateTime(new Date());
		jpaDao.save(dictData);
	}

	@Override
	@Transactional
	public void updateDictData(SysDictData dictData) {
		dictData.setUpdateBy(ShiroUtils.getLoginName());
		dictData.setUpdateTime(new Date());
		jpaDao.update(dictData);
	}
}
