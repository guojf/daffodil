package com.daffodil.system.controller.web;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daffodil.core.annotation.FormToken;
import com.daffodil.core.annotation.FormToken.TokenType;
import com.daffodil.core.annotation.Log;
import com.daffodil.core.annotation.Log.BusinessType;
import com.daffodil.core.controller.BaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableInfo;
import com.daffodil.core.entity.Ztree;
import com.daffodil.system.entity.SysDictType;
import com.daffodil.system.service.ISysDictTypeService;
import com.daffodil.util.ExcelUtils;
import com.daffodil.util.text.Convert;

/**
 * 数据字典控制层
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Controller
@RequestMapping("/system/dict/type")
public class SysDictTypeController extends BaseController {
	
	private String prefix = "system/dict/type";

	@Autowired
	private ISysDictTypeService dictTypeService;

	@RequiresPermissions("system:dict:view")
	@GetMapping()
	public String dictType() {
		return prefix + "/type";
	}

	@SuppressWarnings("unchecked")
	@PostMapping("/list")
	@RequiresPermissions("system:dict:list")
	@ResponseBody
	public TableInfo list(SysDictType dictType) {
		initQuery(dictType, new Page());
		List<SysDictType> list = dictTypeService.selectDictTypeList(query);
		return initTableInfo(list, query);
	}

	@SuppressWarnings("unchecked")
	@Log(title = "字典类型", businessType = BusinessType.EXPORT)
	@RequiresPermissions("system:dict:export")
	@PostMapping("/export")
	@ResponseBody
	public JsonResult export(SysDictType dictType) {
		initQuery(dictType);
		List<SysDictType> list = dictTypeService.selectDictTypeList(query);
		ExcelUtils<SysDictType> util = new ExcelUtils<SysDictType>(SysDictType.class);
		return util.exportExcel(list, "字典类型");
	}

	/**
	 * 新增字典类型
	 */
	@FormToken
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 新增保存字典类型
	 */
	@FormToken(TokenType.DESTROY)
	@Log(title = "字典类型", businessType = BusinessType.INSERT)
	@RequiresPermissions("system:dict:add")
	@PostMapping("/add")
	@ResponseBody
	public JsonResult addSave(@Validated SysDictType dictType) {
		dictTypeService.insertDictType(dictType);
		return JsonResult.success();
	}

	/**
	 * 修改字典类型
	 */
	@GetMapping("/edit/{dictId}")
	public String edit(@PathVariable("dictId") String dictId, ModelMap modelMap) {
		modelMap.put("dict", dictTypeService.selectDictTypeById(dictId));
		return prefix + "/edit";
	}

	/**
	 * 修改保存字典类型
	 */
	@Log(title = "字典类型", businessType = BusinessType.UPDATE)
	@RequiresPermissions("system:dict:edit")
	@PostMapping("/edit")
	@ResponseBody
	public JsonResult editSave(@Validated SysDictType dictType) {
		dictTypeService.updateDictType(dictType);
		return JsonResult.success();
	}

	@Log(title = "字典类型", businessType = BusinessType.DELETE)
	@RequiresPermissions("system:dict:remove")
	@PostMapping("/remove")
	@ResponseBody
	public JsonResult remove(String ids) {
		dictTypeService.deleteDictTypeByIds(Convert.toStrArray(ids));
		return JsonResult.success();
	}

	/**
	 * 查询字典详细
	 */
	@SuppressWarnings("unchecked")
	@RequiresPermissions("system:dict:list")
	@GetMapping("/detail/{dictId}")
	public String detail(@PathVariable("dictId") String dictId, ModelMap modelMap) {
		modelMap.put("dict", dictTypeService.selectDictTypeById(dictId));
		initQuery(new SysDictType());
		modelMap.put("dictList", dictTypeService.selectDictTypeList(query));
		return "system/dict/data/data";
	}

	/**
	 * 校验字典类型
	 */
	@PostMapping("/checkDictTypeUnique")
	@ResponseBody
	public String checkDictTypeUnique(SysDictType dictType) {
		return dictTypeService.checkDictTypeUnique(dictType) ? "1" : "0";
	}

	/**
	 * 选择字典树
	 */
	@GetMapping("/selectDictTree/{columnId}/{dictType}")
	public String selectDeptTree(@PathVariable("columnId") String columnId, @PathVariable("dictType") String dictType, ModelMap modelMap) {
		modelMap.put("columnId", columnId);
		modelMap.put("dict", dictTypeService.selectDictTypeByType(dictType));
		return prefix + "/tree";
	}

	/**
	 * 加载字典列表树
	 */
	@GetMapping("/treeData")
	@ResponseBody
	public List<Ztree> treeData() {
		List<Ztree> ztrees = dictTypeService.selectDictTree(new SysDictType());
		return ztrees;
	}
}
