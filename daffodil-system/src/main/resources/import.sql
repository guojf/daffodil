-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: daffodil
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_notice`
--

DROP TABLE IF EXISTS `sys_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_notice` (
  `notice_id` varchar(255) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `notice_content` varchar(4000) DEFAULT NULL,
  `notice_title` varchar(50) DEFAULT NULL,
  `notice_type` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_notice`
--

LOCK TABLES `sys_notice` WRITE;
/*!40000 ALTER TABLE `sys_notice` DISABLE KEYS */;
INSERT INTO `sys_notice` VALUES ('40283e81704c759101704c823917001b','admin','2020-02-16 13:39:28','<p>本系统是基于SpringBoot的后台管理系统 易读易懂、界面简洁美观。 采用技术SpringBoot、Hibernate、Shiro、Flowable、thymeleaf等。</p><p>本系统仅作为学习使用目的，不作为商业使用的目的。若使用本系统作为商业活动，导致一切后果自行负责。<br></p>','使用Daffodil后台管理系统公告说明','2',NULL,'0',NULL,NULL);
/*!40000 ALTER TABLE `sys_notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role` (
  `user_post_id` varchar(255) NOT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES ('40283e817048902901704892338d0003','402855816f1e2a2e016f1e2a45510000','402855816f1e2a2e016f1e2a45af0001'),('40283e81704890290170489c65b50014','40283e816f92646d016f92705173001a','40283e817048902901704898b1690005'),('40283e81704890290170489cc7f70019','402855816f329db1016f32a226ef0005','40283e81704890290170489a989f000b'),('40283e81704890290170489cddb0001b','402855816f329db1016f32a226ef0005','40283e817048902901704899f54e0008'),('40283e8170489029017048a0fa9b0020','40283e816f92646d016f92705173001a','40283e8170489029017048a0d993001d'),('40283e8170489029017048a2ea790025','402855816f329db1016f32a226ef0005','40283e8170489029017048a2c9610022'),('40283e8170489029017048ac11570039','40283e816f92646d016f926a76fd0003','40283e8170489029017048a4533b0027'),('40283e8170489029017048ac217d003b','40283e816f92646d016f926a76fd0003','40283e8170489029017048a4f302002a'),('40283e8170489029017048ac27b4003d','40283e816f92646d016f926a76fd0003','40283e8170489029017048a63954002d');
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_type` (
  `dict_type_id` varchar(255) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `dict_name` varchar(100) DEFAULT NULL,
  `dict_type` varchar(100) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`dict_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_type`
--

LOCK TABLES `sys_dict_type` WRITE;
/*!40000 ALTER TABLE `sys_dict_type` DISABLE KEYS */;
INSERT INTO `sys_dict_type` VALUES ('297eb3bf6f41395e016f413d951e0002','admin','2019-12-26 16:05:53','菜单类型','sys_menu_type','菜单类型列表','0','admin','2019-12-26 16:05:58'),('40283e816fad93ca016fad9fc99a000c','admin','2020-01-16 17:12:08','流程状态','sys_flow_status','流程状态列表','0',NULL,NULL),('402855816f31c4de016f31c4f40b0000','system','2019-12-23 15:59:49','用户性别','sys_user_sex','用户性别列表','0',NULL,NULL),('402855816f31c4de016f31c4f4a00001','system','2019-12-23 15:59:49','菜单状态','sys_show_hide','菜单状态列表','0',NULL,NULL),('402855816f31c4de016f31c4f4b60002','system','2019-12-23 15:59:49','数据状态','sys_data_status','数据状态列表','0','admin','2019-12-26 11:49:16'),('402855816f31c4de016f31c4f4c10003','system','2019-12-23 15:59:49','系统是否','sys_yes_no','系统是否列表','0',NULL,NULL),('402855816f31c4de016f31c4f4c30004','system','2019-12-23 15:59:49','通知类型','sys_notice_type','通知类型列表','0',NULL,NULL),('402855816f31c4de016f31c4f4c30005','system','2019-12-23 15:59:49','通知状态','sys_notice_status','通知状态列表','0',NULL,NULL),('402855816f31c4de016f31c4f4d20006','system','2019-12-23 15:59:49','操作类型','sys_oper_type','操作类型列表','0',NULL,NULL),('402855816f31c4de016f31c4f4d20007','system','2019-12-23 15:59:49','系统状态','sys_login_status','登录状态列表','0','admin','2019-12-26 11:57:23');
/*!40000 ALTER TABLE `sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `menu_id` varchar(255) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_type` varchar(255) DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `perms` varchar(100) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `visible` varchar(255) DEFAULT NULL,
  `ancestors` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES ('40283e816f8d4089016f8d420c4c0003','admin','2020-01-10 10:21:54','fa fa-sliders','流程管理','catalog',2,'root','',NULL,'menuItem','admin','2020-01-10 10:22:38','','0',',root'),('40283e816f8d4089016f8d43534b0006','admin','2020-01-10 10:23:17','','流程模板','menu',1,'40283e816f8d4089016f8d420c4c0003','flowable:modeler:view',NULL,'menuItem','admin','2020-02-11 21:18:27','/flowable/modeler','0',',root,40283e816f8d4089016f8d420c4c0003'),('40283e816f8ed7d2016f8ed942780001','admin','2020-01-10 17:46:41','fa fa-gift','流程演示','catalog',4,'root','',NULL,'menuItem','admin','2020-02-15 18:50:50','','0',',root'),('40283e816f8ed7d2016f8eda4eb00003','admin','2020-01-10 17:47:49','','报销管理','menu',1,'40283e816f8ed7d2016f8ed942780001','demo:expense:view',NULL,'menuItem',NULL,NULL,'/demo/expense','0',NULL),('40283e816f8ed7d2016f8edb2ed20005','admin','2020-01-10 17:48:47','','报销查询','button',1,'40283e816f8ed7d2016f8eda4eb00003','demo:expense:list',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e816f8ed7d2016f8edb81d10007','admin','2020-01-10 17:49:08','','报销新增','button',2,'40283e816f8ed7d2016f8eda4eb00003','demo:expense:add',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e816f8ed7d2016f8edbda4e0009','admin','2020-01-10 17:49:31','','报销修改','button',3,'40283e816f8ed7d2016f8eda4eb00003','demo:expense:edit',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e816f8ed7d2016f8edc1cf9000b','admin','2020-01-10 17:49:48','','报销删除','button',4,'40283e816f8ed7d2016f8eda4eb00003','demo:expense:remove',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e816f94047b016f9405ae250001','admin','2020-01-11 17:53:18','fa fa-database','发布管理','catalog',3,'40283e816f8d4089016f8d420c4c0003','',NULL,'menuItem','admin','2020-02-11 18:29:30','','0',',root,40283e816f8d4089016f8d420c4c0003'),('40283e816f9d0921016f9d12329a0009','admin','2020-01-13 12:03:33','','新增','button',1,'40283e816f8d4089016f8d43534b0006','system:modeler:add',NULL,'menuItem','admin','2020-01-13 16:10:07','','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f8d4089016f8d43534b0006'),('40283e816f9d0921016f9d129946000b','admin','2020-01-13 12:03:59','','修改','button',2,'40283e816f8d4089016f8d43534b0006','flowable:modeler:edit',NULL,'menuItem','admin','2020-01-13 16:10:32','','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f8d4089016f8d43534b0006'),('40283e816f9d0921016f9d12e584000d','admin','2020-01-13 12:04:19','','删除','button',3,'40283e816f8d4089016f8d43534b0006','flowable:modeler:remove',NULL,'menuItem','admin','2020-01-13 16:10:51','','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f8d4089016f8d43534b0006'),('40283e816f9de0d5016f9dee83d90008','admin','2020-01-13 16:04:12','','预览','button',4,'40283e816f8d4089016f8d43534b0006','flowable:modeler:image',NULL,'menuItem','admin','2020-02-11 21:18:50','','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f8d4089016f8d43534b0006'),('40283e816f9de0d5016f9deef439000a','admin','2020-01-13 16:04:41','','发布','button',3,'40283e816f94047b016f9405ae250001','flowable:process:publish',NULL,'menuItem','admin','2020-01-13 16:11:36','','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f94047b016f9405ae250001'),('40283e816f9de0d5016f9def3ad9000c','admin','2020-01-13 16:04:59','','取消发布','button',4,'40283e816f94047b016f9405ae250001','flowable:process:unPublish',NULL,'menuItem','admin','2020-01-13 16:11:42','','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f94047b016f9405ae250001'),('40283e816fad93ca016fad95619f0004','admin','2020-01-16 17:00:46','fa fa-envelope-o','任务管理','catalog',3,'root','',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e816fad93ca016fad964b710006','admin','2020-01-16 17:01:46','fa fa-address-book','我的任务','catalog',1,'40283e816fad93ca016fad95619f0004','',NULL,'menuItem','admin','2020-01-18 10:54:28','','0','null,40283e816fad93ca016fad95619f0004'),('40283e816fad93ca016fad96d4db0008','admin','2020-01-16 17:02:21','','待办任务','menu',2,'40283e816fad93ca016fad95619f0004','flowable:task:view',NULL,'menuItem','admin','2020-02-12 17:06:09','/flowable/task/unfinished','0','null,40283e816fad93ca016fad95619f0004'),('40283e816fad93ca016fad97367b000a','admin','2020-01-16 17:02:46','','已办任务','menu',3,'40283e816fad93ca016fad95619f0004','flowable:task:view',NULL,'menuItem','admin','2020-02-12 17:06:18','/flowable/task/finished','0','null,40283e816fad93ca016fad95619f0004'),('40283e816fad93ca016fada7346d0035','admin','2020-01-16 17:20:14','','任务查询','button',5,'40283e816fad93ca016fad95619f0004','flowable:task:list',NULL,'menuItem','admin','2020-01-19 17:17:27','','0','null,40283e816fad93ca016fad95619f0004'),('40283e816fad93ca016fada787940037','admin','2020-01-16 17:20:35','','任务办理','button',6,'40283e816fad93ca016fad95619f0004','flowable:task:edit',NULL,'menuItem','admin','2020-01-19 17:17:34','','0','null,40283e816fad93ca016fad95619f0004'),('40283e816fb691df016fb6936b730003','admin','2020-01-18 10:55:12','','待提交','menu',1,'40283e816fad93ca016fad964b710006','flowable:task:view',NULL,'menuItem','admin','2020-02-12 17:10:15','/flowable/task/myself/0','0','null,40283e816fad93ca016fad95619f0004,40283e816fad93ca016fad964b710006'),('40283e816fb691df016fb6947ace0007','admin','2020-01-18 10:56:22','','已办结','menu',3,'40283e816fad93ca016fad964b710006','flowable:task:view',NULL,'menuItem','admin','2020-02-12 17:10:36','/flowable/task/myself/2','0','null,40283e816fad93ca016fad95619f0004,40283e816fad93ca016fad964b710006'),('40283e816fb8fb58016fb8fcca990002','admin','2020-01-18 22:09:32','','已提交','menu',2,'40283e816fad93ca016fad964b710006','flowable:common:view',NULL,'menuItem','admin','2020-02-12 17:10:25','/flowable/task/myself/1','0','null,40283e816fad93ca016fad95619f0004,40283e816fad93ca016fad964b710006'),('40283e817033b5f7017033b783410001','admin','2020-02-11 18:07:10','','查询','button',1,'402880e46f9ecae8016f9ecc9c7e0001','flowable:user:list',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e817033b5f7017033cbd3fe0005','admin','2020-02-11 18:29:21','','流程类型','menu',2,'40283e816f8d4089016f8d420c4c0003','flowable:type:view',NULL,'menuItem',NULL,NULL,'/flowable/type','0',NULL),('40283e817034009f01703401fd1d0001','admin','2020-02-11 19:28:30','','查询','button',1,'40283e817033b5f7017033cbd3fe0005','flowable:type:list',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e817034009f017034023c610003','admin','2020-02-11 19:28:47','','新增','button',2,'40283e817033b5f7017033cbd3fe0005','flowable:type:add',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e817034009f01703402814d0005','admin','2020-02-11 19:29:04','','修改','button',3,'40283e817033b5f7017033cbd3fe0005','flowable:type:edit',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e817034009f01703402c9210007','admin','2020-02-11 19:29:23','','删除','button',4,'40283e817033b5f7017033cbd3fe0005','flowable:type:remove',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e8170470c040170470dc5c90001','admin','2020-02-15 12:14:10','','约束管理','menu',7,'402855816f31c4de016f31c4f6260024','system:filter:view',NULL,'menuItem',NULL,NULL,'/system/filter','0',NULL),('40283e8170470c040170470fab270006','admin','2020-02-15 12:16:14','','约束查询','button',1,'40283e8170470c040170470dc5c90001','system:filter:list',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e8170470c04017047101c110008','admin','2020-02-15 12:16:43','','约束新增','button',2,'40283e8170470c040170470dc5c90001','system:filter:add',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e8170470c04017047107206000a','admin','2020-02-15 12:17:05','','约束修改','button',3,'40283e8170470c040170470dc5c90001','system:filter:edit',NULL,'menuItem',NULL,NULL,'','0',NULL),('40283e8170470c0401704710b2d8000c','admin','2020-02-15 12:17:22','','约束删除','button',4,'40283e8170470c040170470dc5c90001','system:filter:remove',NULL,'menuItem',NULL,NULL,'','0',NULL),('402855816f31c4de016f31c4f6260024','system','2019-12-23 15:59:49','fa fa-gear','系统管理','catalog',1,'root','','系统管理目录','menuItem','admin','2019-12-25 15:43:30','','0',',root'),('402855816f31c4de016f31c4f6350025','system','2019-12-23 15:59:49','fa fa-video-camera','系统监控','catalog',5,'root','','系统监控目录','menuItem','admin','2020-02-15 18:50:36','','0',',root'),('402855816f31c4de016f31c4f6470027','system','2019-12-23 15:59:49','','用户管理','menu',1,'402855816f31c4de016f31c4f6260024','system:user:view','用户管理菜单','menuItem','admin','2019-12-25 15:43:53','/system/user','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f6570028','system','2019-12-23 15:59:49','','角色管理','menu',2,'402855816f31c4de016f31c4f6260024','system:role:view','角色管理菜单','menuItem','admin','2019-12-25 15:44:23','/system/role','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f6580029','system','2019-12-23 15:59:49','','菜单管理','menu',5,'402855816f31c4de016f31c4f6260024','system:menu:view','菜单管理菜单','menuItem','admin','2019-12-25 15:56:31','/system/menu','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f668002a','system','2019-12-23 15:59:49','','部门管理','menu',3,'402855816f31c4de016f31c4f6260024','system:dept:view','部门管理菜单','menuItem','admin','2019-12-25 15:56:11','/system/dept','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f67a002b','system','2019-12-23 15:59:49','','岗位管理','menu',4,'402855816f31c4de016f31c4f6260024','system:post:view','岗位管理菜单','menuItem','admin','2019-12-25 15:56:23','/system/post','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f67a002c','system','2019-12-23 15:59:49','','字典管理','menu',6,'402855816f31c4de016f31c4f6260024','system:dict:view','字典管理菜单','menuItem','admin','2019-12-26 10:07:54','/system/dict/type','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f689002d','system','2019-12-23 15:59:49','','参数设置','menu',8,'402855816f31c4de016f31c4f6260024','system:config:view','参数设置菜单','menuItem','admin','2020-02-15 12:15:00','/system/config','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f69b002e','system','2019-12-23 15:59:49','','通知公告','menu',9,'402855816f31c4de016f31c4f6260024','system:notice:view','通知公告菜单','menuItem','admin','2020-02-15 12:15:10','/system/notice','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f6a5002f','system','2019-12-23 15:59:49','','日志管理','catalog',10,'402855816f31c4de016f31c4f6260024','','日志管理菜单','menuItem','admin','2020-02-15 12:15:25','','0',',root,402855816f31c4de016f31c4f6260024'),('402855816f31c4de016f31c4f6ae0030','system','2019-12-23 15:59:49','','在线用户','menu',1,'402855816f31c4de016f31c4f6350025','monitor:online:view','在线用户菜单','menuItem','admin','2019-12-25 15:45:48','/monitor/online','0',',root,402855816f31c4de016f31c4f6350025'),('402855816f31c4de016f31c4f6bd0031','system','2019-12-23 15:59:49','','服务监控','menu',3,'402855816f31c4de016f31c4f6350025','monitor:server:view','服务监控菜单','menuItem','admin','2019-12-25 15:45:52','/monitor/server','0',',root,402855816f31c4de016f31c4f6350025'),('402855816f31c4de016f31c4f6e20035','system','2019-12-23 15:59:49','','操作日志','menu',1,'402855816f31c4de016f31c4f6a5002f','monitor:operlog:view','操作日志菜单','menuItem','admin','2019-12-25 15:46:34','/monitor/operlog','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f'),('402855816f31c4de016f31c4f6e70036','system','2019-12-23 15:59:49','','登录日志','menu',2,'402855816f31c4de016f31c4f6a5002f','monitor:logininfo:view','登录日志菜单','menuItem','admin','2019-12-25 15:46:39','/monitor/logininfo','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f'),('402855816f31c4de016f31c4f6ec0037','system','2019-12-23 15:59:49','','用户查询','button',1,'402855816f31c4de016f31c4f6470027','system:user:list','','menuItem','admin','2019-12-25 15:44:07','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027'),('402855816f31c4de016f31c4f6f30038','system','2019-12-23 15:59:49','','用户新增','button',2,'402855816f31c4de016f31c4f6470027','system:user:add','','menuItem','admin','2019-12-25 15:46:49','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027'),('402855816f31c4de016f31c4f6f90039','system','2019-12-23 15:59:49','','用户修改','button',3,'402855816f31c4de016f31c4f6470027','system:user:edit','','menuItem','admin','2019-12-25 15:46:53','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027'),('402855816f31c4de016f31c4f6fe003a','system','2019-12-23 15:59:50','','用户删除','button',4,'402855816f31c4de016f31c4f6470027','system:user:remove','','menuItem','admin','2019-12-25 15:47:07','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027'),('402855816f31c4de016f31c4f703003b','system','2019-12-23 15:59:50','','用户导出','button',5,'402855816f31c4de016f31c4f6470027','system:user:export','','menuItem','admin','2019-12-25 15:47:12','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027'),('402855816f31c4de016f31c4f709003c','system','2019-12-23 15:59:50','','用户导入','button',6,'402855816f31c4de016f31c4f6470027','system:user:import','','menuItem','admin','2019-12-25 15:47:17','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027'),('402855816f31c4de016f31c4f710003d','system','2019-12-23 15:59:50','','重置密码','button',7,'402855816f31c4de016f31c4f6470027','system:user:resetPwd','','menuItem','admin','2019-12-25 15:47:21','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6470027'),('402855816f31c4de016f31c4f716003e','system','2019-12-23 15:59:50','','角色查询','button',1,'402855816f31c4de016f31c4f6570028','system:role:list','','menuItem','admin','2019-12-25 15:47:37','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028'),('402855816f31c4de016f31c4f71d003f','system','2019-12-23 15:59:50','','角色新增','button',2,'402855816f31c4de016f31c4f6570028','system:role:add','','menuItem','admin','2019-12-25 15:49:21','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028'),('402855816f31c4de016f31c4f7240040','system','2019-12-23 15:59:50','','角色修改','button',3,'402855816f31c4de016f31c4f6570028','system:role:edit','','menuItem','admin','2019-12-25 15:49:26','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028'),('402855816f31c4de016f31c4f7290041','system','2019-12-23 15:59:50','','角色删除','button',4,'402855816f31c4de016f31c4f6570028','system:role:remove','','menuItem','admin','2019-12-25 15:49:32','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028'),('402855816f31c4de016f31c4f72f0042','system','2019-12-23 15:59:50','','角色导出','button',5,'402855816f31c4de016f31c4f6570028','system:role:export','','menuItem','admin','2019-12-25 15:49:37','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6570028'),('402855816f31c4de016f31c4f7340043','system','2019-12-23 15:59:50','','菜单查询','button',1,'402855816f31c4de016f31c4f6580029','system:menu:list','','menuItem','admin','2019-12-25 15:47:59','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6580029'),('402855816f31c4de016f31c4f7340044','system','2019-12-23 15:59:50','','菜单新增','button',2,'402855816f31c4de016f31c4f6580029','system:menu:add','','menuItem','admin','2019-12-25 15:48:06','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6580029'),('402855816f31c4de016f31c4f7450045','system','2019-12-23 15:59:50','','菜单修改','button',3,'402855816f31c4de016f31c4f6580029','system:menu:edit','','menuItem','admin','2019-12-25 15:47:48','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6580029'),('402855816f31c4de016f31c4f74b0046','system','2019-12-23 15:59:50','','菜单删除','button',4,'402855816f31c4de016f31c4f6580029','system:menu:remove','','menuItem','admin','2019-12-25 15:48:19','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6580029'),('402855816f31c4de016f31c4f7530047','system','2019-12-23 15:59:50','','部门查询','button',1,'402855816f31c4de016f31c4f668002a','system:dept:list','','menuItem','admin','2019-12-25 15:50:01','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a'),('402855816f31c4de016f31c4f7590048','system','2019-12-23 15:59:50','','部门新增','button',2,'402855816f31c4de016f31c4f668002a','system:dept:add','','menuItem','admin','2019-12-25 15:50:07','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a'),('402855816f31c4de016f31c4f75f0049','system','2019-12-23 15:59:50','','部门修改','button',3,'402855816f31c4de016f31c4f668002a','system:dept:edit','','menuItem','admin','2019-12-25 15:50:12','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a'),('402855816f31c4de016f31c4f765004a','system','2019-12-23 15:59:50','','部门删除','button',4,'402855816f31c4de016f31c4f668002a','system:dept:remove','','menuItem','admin','2019-12-25 15:49:04','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f668002a'),('402855816f31c4de016f31c4f76c004b','system','2019-12-23 15:59:50','','岗位查询','button',1,'402855816f31c4de016f31c4f67a002b','system:post:list','','menuItem','admin','2019-12-25 15:48:30','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b'),('402855816f31c4de016f31c4f773004c','system','2019-12-23 15:59:50','','岗位新增','button',2,'402855816f31c4de016f31c4f67a002b','system:post:add','','menuItem','admin','2019-12-25 15:48:37','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b'),('402855816f31c4de016f31c4f775004d','system','2019-12-23 15:59:50','','岗位修改','button',3,'402855816f31c4de016f31c4f67a002b','system:post:edit','','menuItem','admin','2019-12-25 15:48:48','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b'),('402855816f31c4de016f31c4f775004e','system','2019-12-23 15:59:50','','岗位删除','button',4,'402855816f31c4de016f31c4f67a002b','system:post:remove','','menuItem','admin','2019-12-25 15:48:58','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b'),('402855816f31c4de016f31c4f788004f','system','2019-12-23 15:59:50','','岗位导出','button',5,'402855816f31c4de016f31c4f67a002b','system:post:export','','menuItem','admin','2019-12-25 15:51:15','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002b'),('402855816f31c4de016f31c4f7900050','system','2019-12-23 15:59:50','','字典查询','button',1,'402855816f31c4de016f31c4f67a002c','system:dict:list','','menuItem','admin','2019-12-25 15:50:29','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002c'),('402855816f31c4de016f31c4f7980051','system','2019-12-23 15:59:50','','字典新增','button',2,'402855816f31c4de016f31c4f67a002c','system:dict:add','','menuItem','admin','2019-12-25 15:50:34','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002c'),('402855816f31c4de016f31c4f79c0052','system','2019-12-23 15:59:50','','字典修改','button',3,'402855816f31c4de016f31c4f67a002c','system:dict:edit','','menuItem','admin','2019-12-25 15:50:39','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002c'),('402855816f31c4de016f31c4f79c0053','system','2019-12-23 15:59:50','','字典删除','button',4,'402855816f31c4de016f31c4f67a002c','system:dict:remove','','menuItem','admin','2019-12-25 15:50:47','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002c'),('402855816f31c4de016f31c4f7ae0054','system','2019-12-23 15:59:50','','字典导出','button',5,'402855816f31c4de016f31c4f67a002c','system:dict:export','','menuItem','admin','2019-12-25 15:50:53','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f67a002c'),('402855816f31c4de016f31c4f7ae0055','system','2019-12-23 15:59:50','','参数查询','button',1,'402855816f31c4de016f31c4f689002d','system:config:list','','menuItem','admin','2019-12-25 15:51:24','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f689002d'),('402855816f31c4de016f31c4f7bd0056','system','2019-12-23 15:59:50','','参数新增','button',2,'402855816f31c4de016f31c4f689002d','system:config:add','','menuItem','admin','2019-12-25 15:51:29','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f689002d'),('402855816f31c4de016f31c4f7bd0057','system','2019-12-23 15:59:50','','参数修改','button',3,'402855816f31c4de016f31c4f689002d','system:config:edit','','menuItem','admin','2019-12-25 15:51:34','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f689002d'),('402855816f31c4de016f31c4f7bd0058','system','2019-12-23 15:59:50','','参数删除','button',4,'402855816f31c4de016f31c4f689002d','system:config:remove','','menuItem','admin','2019-12-25 15:53:09','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f689002d'),('402855816f31c4de016f31c4f7cd0059','system','2019-12-23 15:59:50','','参数导出','button',5,'402855816f31c4de016f31c4f689002d','system:config:export','','menuItem','admin','2019-12-25 15:53:15','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f689002d'),('402855816f31c4de016f31c4f7cd005a','system','2019-12-23 15:59:50','','公告查询','button',1,'402855816f31c4de016f31c4f69b002e','system:notice:list','','menuItem','admin','2019-12-25 15:51:40','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e'),('402855816f31c4de016f31c4f7cd005b','system','2019-12-23 15:59:50','','公告新增','button',2,'402855816f31c4de016f31c4f69b002e','system:notice:add','','menuItem','admin','2019-12-25 15:51:45','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e'),('402855816f31c4de016f31c4f7e2005c','system','2019-12-23 15:59:50','','公告修改','button',3,'402855816f31c4de016f31c4f69b002e','system:notice:edit','','menuItem','admin','2019-12-25 15:51:50','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e'),('402855816f31c4de016f31c4f7e7005d','system','2019-12-23 15:59:50','','公告删除','button',4,'402855816f31c4de016f31c4f69b002e','system:notice:remove','','menuItem','admin','2019-12-25 15:51:55','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f69b002e'),('402855816f31c4de016f31c4f7ed005e','system','2019-12-23 15:59:50','','操作查询','button',1,'402855816f31c4de016f31c4f6e20035','monitor:operlog:list','','menuItem','admin','2019-12-25 15:52:03','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f,402855816f31c4de016f31c4f6e20035'),('402855816f31c4de016f31c4f7f2005f','system','2019-12-23 15:59:50','','操作删除','button',2,'402855816f31c4de016f31c4f6e20035','monitor:operlog:remove','','menuItem','admin','2019-12-25 15:52:09','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f,402855816f31c4de016f31c4f6e20035'),('402855816f31c4de016f31c4f7f80060','system','2019-12-23 15:59:50','','详细信息','button',3,'402855816f31c4de016f31c4f6e20035','monitor:operlog:detail','','menuItem','admin','2019-12-25 15:52:16','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f,402855816f31c4de016f31c4f6e20035'),('402855816f31c4de016f31c4f7ff0061','system','2019-12-23 15:59:50','','日志导出','button',4,'402855816f31c4de016f31c4f6e20035','monitor:operlog:export','','menuItem','admin','2019-12-25 15:52:23','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f,402855816f31c4de016f31c4f6e20035'),('402855816f31c4de016f31c4f8060062','system','2019-12-23 15:59:50','','登录查询','button',1,'402855816f31c4de016f31c4f6e70036','monitor:logininfo:list','','menuItem','admin','2019-12-25 15:52:34','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f,402855816f31c4de016f31c4f6e70036'),('402855816f31c4de016f31c4f80b0063','system','2019-12-23 15:59:50','','登录删除','button',2,'402855816f31c4de016f31c4f6e70036','monitor:logininfo:remove','','menuItem','admin','2019-12-25 15:52:41','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f,402855816f31c4de016f31c4f6e70036'),('402855816f31c4de016f31c4f8110064','system','2019-12-23 15:59:50','','日志导出','button',3,'402855816f31c4de016f31c4f6e70036','monitor:logininfo:export','','menuItem','admin','2019-12-25 15:52:48','#','0',',root,402855816f31c4de016f31c4f6260024,402855816f31c4de016f31c4f6a5002f,402855816f31c4de016f31c4f6e70036'),('402855816f31c4de016f31c4f8180065','system','2019-12-23 15:59:50','','在线查询','button',1,'402855816f31c4de016f31c4f6ae0030','monitor:online:list','','menuItem','admin','2019-12-25 15:46:18','#','0',',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6ae0030'),('402855816f31c4de016f31c4f81e0066','system','2019-12-23 15:59:50','','批量强退','button',2,'402855816f31c4de016f31c4f6ae0030','monitor:online:batchForceLogout','','menuItem','admin','2019-12-25 15:46:22','#','0',',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6ae0030'),('402855816f31c4de016f31c4f8240067','system','2019-12-23 15:59:50','','单条强退','button',3,'402855816f31c4de016f31c4f6ae0030','monitor:online:forceLogout','','menuItem','admin','2019-12-25 15:46:27','#','0',',root,402855816f31c4de016f31c4f6350025,402855816f31c4de016f31c4f6ae0030'),('402880e46f9812b4016f981912eb0003','admin','2020-01-12 12:52:58','','已发布','menu',2,'40283e816f94047b016f9405ae250001','flowable:modeler:view',NULL,'menuItem','admin','2020-02-11 21:19:55','/flowable/modeler/deploy','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f94047b016f9405ae250001'),('402880e46f9812b4016f9819a6f00005','admin','2020-01-12 12:53:36','','待发布','menu',1,'40283e816f94047b016f9405ae250001','flowable:modeler:view',NULL,'menuItem','admin','2020-02-11 21:19:34','/flowable/modeler/undeploy','0',',root,40283e816f8d4089016f8d420c4c0003,40283e816f94047b016f9405ae250001'),('402880e46f9ecae8016f9ecc9c7e0001','admin','2020-01-13 20:06:47','','绑定流程','menu',4,'40283e816f8d4089016f8d420c4c0003','flowable:user:view',NULL,'menuItem','admin','2020-02-11 18:29:40','/flowable/user','0',',root,40283e816f8d4089016f8d420c4c0003'),('402881836fbcd7d6016fbcda617a0005','admin','2020-01-19 16:10:26','','查看流程图','button',7,'40283e816fad93ca016fad95619f0004','flowable:task:image',NULL,'menuItem','admin','2020-01-19 17:17:41','','0','null,40283e816fad93ca016fad95619f0004'),('402881836fbd0217016fbd18b3950004','admin','2020-01-19 17:18:30','','办结任务','menu',4,'40283e816fad93ca016fad95619f0004','flowable:task:view',NULL,'menuItem','admin','2020-02-12 17:06:28','/flowable/task/complete','0','null,40283e816fad93ca016fad95619f0004'),('402881836fc0c451016fc0c5a1490001','admin','2020-01-20 10:26:15','','流程步骤详情','button',8,'40283e816fad93ca016fad95619f0004','flowable:task:detail',NULL,'menuItem',NULL,NULL,'','0',NULL);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_post`
--

DROP TABLE IF EXISTS `sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_post` (
  `post_id` varchar(255) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  `post_code` varchar(64) DEFAULT NULL,
  `post_name` varchar(50) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_post`
--

LOCK TABLES `sys_post` WRITE;
/*!40000 ALTER TABLE `sys_post` DISABLE KEYS */;
INSERT INTO `sys_post` VALUES ('40283e81704c759101704c78a7e70011','admin','2020-02-16 13:29:01',1,'000','董事长','','0',NULL,NULL),('40283e81704c759101704c78d7410013','admin','2020-02-16 13:29:13',2,'100','总经理','','0',NULL,NULL),('40283e81704c759101704c79275e0015','admin','2020-02-16 13:29:33',3,'200','部门经理','','0',NULL,NULL),('40283e81704c759101704c798f1e0017','admin','2020-02-16 13:30:00',4,'300','员工','','0',NULL,NULL);
/*!40000 ALTER TABLE `sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `config_id` varchar(255) NOT NULL,
  `config_key` varchar(100) DEFAULT NULL,
  `config_name` varchar(100) DEFAULT NULL,
  `config_type` varchar(255) DEFAULT NULL,
  `config_value` varchar(500) DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES ('40283e817047a079017047a4a9d00001','shiro.kaptcha.type','验证码类型','Y','default','admin','2020-02-16 13:31:49','设置验证码类型，默认字符default，数学math','admin','2020-02-15 15:12:45'),('40283e817047a079017047a6db560009','shiro.kaptcha.enabled','验证码开关','Y','false','admin','2020-02-16 13:31:49','设置验证码开关，开true，关false','admin','2020-02-15 16:25:10'),('40283e817047a079017047a81f7f000b','shiro.cookie.domain','Cookie的域名 ','Y','','admin','2020-02-16 13:31:49','设置Cookie的域名 默认空，即当前访问的域名','admin','2020-02-15 15:10:13'),('40283e817047a079017047a872e3000d','shiro.cookie.path','Cookie的有效访问路径','Y','/','admin','2020-02-16 13:31:49','设置cookie的有效访问路径','admin','2020-02-15 15:10:32'),('40283e817047a079017047a8f073000f','shiro.cookie.httpOnly','Cookie的HttpOnly属性','Y','true','admin','2020-02-16 13:31:49','设置HttpOnly属性','admin','2020-02-15 15:10:40'),('40283e817047a079017047a94a300011','shiro.cookie.maxAge','Cookie的过期时间','Y','30','admin','2020-02-16 13:31:49','设置Cookie的过期时间，天为单位','admin','2020-02-15 15:12:02'),('40283e817047a079017047a9a3800013','shiro.session.expireTime','会话超时时间','Y','30','admin','2020-02-16 13:31:49','设置会话超时时间（默认30分钟）','admin','2020-02-15 15:11:04'),('40283e817047a079017047a9fdef0015','shiro.session.maxSession','最大会话数','Y','-1','admin','2020-02-16 13:31:49','设置同一个用户登录的最大会话数，比如2的意思是同一个用户账号允许最多同时两个人登录（默认-1不限制）','admin','2020-02-15 15:11:50'),('40283e817047a079017047ab7dca0017','shiro.session.kickoutAfter','是否踢出用户','Y','false','admin','2020-02-16 13:31:49','是否踢出之前登录的/之后登录的用户，默认踢出之前登录的用户','admin','2020-02-15 15:06:39'),('40283e817047e7ff017047efed6a0001','shiro.session.kickoutUrl','踢出后重定向地址','Y','/login','admin','2020-02-16 13:31:49','设置踢出后重定向到的地址','admin','2020-02-15 16:21:28'),('40283e817049821e0170498342420001','shiro.ehcache.name','Ehcache缓存名称','Y','daffodil','admin','2020-02-16 13:31:49','设置Ehcache缓存名称','admin','2020-02-16 12:32:30'),('40283e81704c421b01704c433bd70001','shiro.user.loginUrl','系统登录地址','Y','/login','admin','2020-02-16 13:31:49','设置系统登录地址','admin','2020-02-16 12:32:19'),('40283e81704c421b01704c43e4060003','shiro.user.indexUrl','系统登录首页地址','Y','/index','admin','2020-02-16 13:31:49','设置系统登录首页地址','admin','2020-02-16 12:32:16'),('40283e81704c421b01704c4456800005','shiro.user.unauthorizedUrl','权限认证失败跳转地址','Y','/unauth','admin','2020-02-16 13:31:49','设置权限认证失败跳转地址','admin','2020-02-16 12:32:13'),('40283e81704c759101704c7b3a550019','shiro.cookie.name','Cookie的名称','Y','rememberMe','admin','2020-02-16 13:31:49','设置Cookie的名称',NULL,NULL),('402855816f18b1e3016f18bbb5c50000','sys.user.initPassword','用户管理-账号初始密码','N','123456','admin','2020-02-16 13:31:49','用户管理的系统用户账号初始密码为 123456','admin','2020-02-15 16:23:42'),('402855816f18b1e3016f18d5a9970003','sys.index.sideTheme','主框架页-侧边栏主题','N','theme-dark','admin','2020-02-16 13:31:49','深色主题theme-dark，浅色主题theme-light','admin','2020-02-15 16:23:36'),('402855816f18b1e3016f18d612bd0004','sys.index.skinName','主框架页-默认皮肤样式名称','N','skin-red','admin','2020-02-16 13:31:49','蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow','admin','2020-02-15 16:23:30');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept`
--

DROP TABLE IF EXISTS `sys_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dept` (
  `dept_id` varchar(255) NOT NULL,
  `ancestors` varchar(255) DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `dept_name` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `leader` varchar(255) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept`
--

LOCK TABLES `sys_dept` WRITE;
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` VALUES ('40283e816f7dcf4f016f7ddb08dd001e',',root,402855816f35ac13016f35cc740b0017','admin','2020-01-07 10:35:04','项目美工部','','','402855816f35ac13016f35cc740b0017','','部门','0','admin','2020-01-07 10:42:19',2),('40283e816f7dcf4f016f7dde4c710021',',root,402855816f35ac13016f35cc740b0017,40283e816f7dcf4f016f7ddb08dd001e','admin','2020-01-07 10:38:38','美工一组','','','40283e816f7dcf4f016f7ddb08dd001e','','部门小组','0','admin','2020-01-07 10:42:25',1),('40283e816f7dcf4f016f7de00ef30023',',root,402855816f35ac13016f35cc740b0017,40283e816f7dcf4f016f7ddb08dd001e','admin','2020-01-07 10:40:34','美工二组','','','40283e816f7dcf4f016f7ddb08dd001e','','部门小组','0','admin','2020-01-07 10:42:30',2),('40283e816f7dcf4f016f7de064990027',',root,402855816f35ac13016f35cc740b0017,402855816f35ac13016f35ce3bca0019','admin','2020-01-07 10:40:55','研发三组','','','402855816f35ac13016f35ce3bca0019','','部门小组','0','admin','2020-01-07 10:42:12',3),('40283e816f92646d016f926c38e4000f',',root,402855816f35ac13016f35cc740b0017','admin','2020-01-11 10:26:04','财务部门','','','402855816f35ac13016f35cc740b0017','','','0',NULL,NULL,3),('402855816f35ac13016f35cc740b0017',',root','admin','2019-12-24 10:46:29','福州Daffodil科技有限公司','Daffodil@163.com','Daffodil','root','18088888888','企业机构','0','admin','2020-02-15 14:39:20',1),('402855816f35ac13016f35ce3bca0019',',root,402855816f35ac13016f35cc740b0017','admin','2019-12-24 15:03:36','项目研发部','','','402855816f35ac13016f35cc740b0017','','部门','0','admin','2020-01-07 10:30:50',1),('402855816f35ac13016f35ce9169001b',',root,402855816f35ac13016f35cc740b0017,402855816f35ac13016f35ce3bca0019','admin','2019-12-24 14:44:58','研发一组','','','402855816f35ac13016f35ce3bca0019','','部门小组','0','admin','2020-01-11 10:28:45',1),('402855816f35ac13016f35ced093001d',',root,402855816f35ac13016f35cc740b0017,402855816f35ac13016f35ce3bca0019','admin','2019-12-24 14:56:43','研发二组','','','402855816f35ac13016f35ce3bca0019','','部门小组','0','admin','2020-01-07 10:40:45',2),('402855816f35e8b6016f35ea04530001',',root,402855816f35fff2016f360af3b6000c','admin','2019-12-24 14:56:59','项目实施部','','','402855816f35fff2016f360af3b6000c','','部门','0','admin','2020-01-07 10:31:31',1),('402855816f35e8b6016f35ea41fb0003',',root,402855816f35fff2016f360af3b6000c,402855816f35e8b6016f35ea04530001','admin','2019-12-24 11:53:45','实施一组','','','402855816f35e8b6016f35ea04530001','','部门小组','0','admin','2020-01-07 10:31:36',1),('402855816f35fff2016f3609e1760007',',root,402855816f35fff2016f360af3b6000c,402855816f35e8b6016f35ea04530001','admin','2019-12-24 14:58:43','实施二组','','','402855816f35e8b6016f35ea04530001','','部门小组','0','admin','2020-01-07 10:31:42',2),('402855816f35fff2016f360af3b6000c',',root','admin','2019-12-24 15:02:10','厦门Daffodil科技有限公司','Daffodil@163.com','Daffodil','root','18088888888','企业机构','0','admin','2020-01-07 10:33:54',2);
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `role_id` varchar(255) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `role_scope` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `role_key` varchar(100) DEFAULT NULL,
  `role_name` varchar(30) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES ('40283e816f92646d016f926a76fd0003','admin','2020-01-11 10:24:08',NULL,'财务审批','FinanceManager','财务管理员','0','admin','2020-02-15 19:48:41',3),('40283e816f92646d016f92705173001a','admin','2020-01-11 10:30:32',NULL,'部门管理员','DivisionManager','部门管理员','0','admin','2020-02-15 19:48:59',4),('402855816f1e2a2e016f1e2a45510000','system','2019-12-19 20:38:04','1','超级管理员','SysAdmin','超级管理员','0','admin','2020-01-11 10:32:43',1),('402855816f329db1016f32a226ef0005','admin','2019-12-19 20:38:04','4','普通管理员','Admin','普通管理员','0','admin','2020-02-15 19:48:19',2);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu` (
  `role_menu_id` varchar(255) NOT NULL,
  `menu_id` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES ('40283e8170489029017048ad8f4c0043','40283e816fad93ca016fad95619f0004','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0044','40283e816fad93ca016fad964b710006','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0045','40283e816fb691df016fb6936b730003','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0046','40283e816fb8fb58016fb8fcca990002','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0047','40283e816fb691df016fb6947ace0007','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0048','40283e816fad93ca016fad96d4db0008','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0049','40283e816fad93ca016fad97367b000a','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c004a','40283e816fad93ca016fada7346d0035','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c004b','40283e816fad93ca016fada787940037','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c004c','402881836fbcd7d6016fbcda617a0005','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c004d','402881836fc0c451016fc0c5a1490001','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c004e','40283e816f8ed7d2016f8ed942780001','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c004f','40283e816f8ed7d2016f8eda4eb00003','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0050','40283e816f8ed7d2016f8edb2ed20005','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0051','40283e816f8ed7d2016f8edb81d10007','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0052','40283e816f8ed7d2016f8edbda4e0009','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ad8f4c0053','40283e816f8ed7d2016f8edc1cf9000b','402855816f329db1016f32a226ef0005'),('40283e8170489029017048ade6030055','40283e816fad93ca016fad95619f0004','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030056','40283e816fad93ca016fad964b710006','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030057','40283e816fb691df016fb6936b730003','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030058','40283e816fb8fb58016fb8fcca990002','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030059','40283e816fb691df016fb6947ace0007','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade603005a','40283e816fad93ca016fad96d4db0008','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade603005b','40283e816fad93ca016fad97367b000a','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade603005c','40283e816fad93ca016fada7346d0035','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade603005d','40283e816fad93ca016fada787940037','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade603005e','402881836fbcd7d6016fbcda617a0005','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade603005f','402881836fc0c451016fc0c5a1490001','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030060','40283e816f8ed7d2016f8ed942780001','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030061','40283e816f8ed7d2016f8eda4eb00003','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030062','40283e816f8ed7d2016f8edb2ed20005','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030063','40283e816f8ed7d2016f8edb81d10007','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030064','40283e816f8ed7d2016f8edbda4e0009','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ade6030065','40283e816f8ed7d2016f8edc1cf9000b','40283e816f92646d016f926a76fd0003'),('40283e8170489029017048ae2cc30067','402855816f31c4de016f31c4f6260024','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30068','402855816f31c4de016f31c4f668002a','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30069','402855816f31c4de016f31c4f7530047','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3006a','402855816f31c4de016f31c4f7590048','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3006b','402855816f31c4de016f31c4f75f0049','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3006c','40283e816fad93ca016fad95619f0004','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3006d','40283e816fad93ca016fad964b710006','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3006e','40283e816fb691df016fb6936b730003','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3006f','40283e816fb8fb58016fb8fcca990002','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30070','40283e816fb691df016fb6947ace0007','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30071','40283e816fad93ca016fad96d4db0008','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30072','40283e816fad93ca016fad97367b000a','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30073','40283e816fad93ca016fada7346d0035','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30074','40283e816fad93ca016fada787940037','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30075','402881836fbcd7d6016fbcda617a0005','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30076','402881836fc0c451016fc0c5a1490001','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30077','40283e816f8ed7d2016f8ed942780001','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30078','40283e816f8ed7d2016f8eda4eb00003','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc30079','40283e816f8ed7d2016f8edb2ed20005','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3007a','40283e816f8ed7d2016f8edb81d10007','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3007b','40283e816f8ed7d2016f8edbda4e0009','40283e816f92646d016f92705173001a'),('40283e8170489029017048ae2cc3007c','40283e816f8ed7d2016f8edc1cf9000b','40283e816f92646d016f92705173001a'),('402881836fc146a7016fc147b5ec0004','40283e816fad93ca016fad95619f0004','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5ec0005','40283e816fad93ca016fad964b710006','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5ec0006','40283e816fb691df016fb6936b730003','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc0007','40283e816fb8fb58016fb8fcca990002','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc0008','40283e816fb691df016fb6947ace0007','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc0009','40283e816fad93ca016fad96d4db0008','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc000a','40283e816fad93ca016fad97367b000a','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc000b','402881836fbd0217016fbd18b3950004','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc000c','40283e816fad93ca016fada7346d0035','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc000d','40283e816fad93ca016fada787940037','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc000e','402881836fbcd7d6016fbcda617a0005','40283e816f92646d016f92758ce50051'),('402881836fc146a7016fc147b5fc000f','402881836fc0c451016fc0c5a1490001','40283e816f92646d016f92758ce50051');
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `user_id` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `dept_id` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `is_admin` varchar(255) DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `login_name` varchar(30) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `role_id` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `user_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES ('40283e817048902901704898b1690005',NULL,'admin','2020-02-15 19:25:31','402855816f35ac13016f35ce3bca0019','caocao@163.com',NULL,NULL,'曹操',NULL,NULL,'47a8a1577ee38161b67a7d41a48dd31f','18088888881','',NULL,'3925a1','0','0','admin','2020-02-15 19:29:34','曹操'),('40283e817048902901704899f54e0008',NULL,'admin','2020-02-15 19:26:54','402855816f35ac13016f35ce9169001b','dianwei@163.com',NULL,NULL,'典韦',NULL,NULL,'14d033ef4c0f62698bfe5c2c62d24bbd','18088888882','',NULL,'9abdbf','0','0','admin','2020-02-15 19:30:05','典韦'),('40283e81704890290170489a989f000b',NULL,'admin','2020-02-15 19:27:36','402855816f35ac13016f35ced093001d','guojia@163.com',NULL,NULL,'郭嘉',NULL,NULL,'a0bfed267ef76cccb2e84e0004c2de36','18088888883','',NULL,'1f0fcf','0','0','admin','2020-02-15 19:29:59','郭嘉'),('40283e8170489029017048a0d993001d',NULL,'admin','2020-02-15 19:34:26','40283e816f7dcf4f016f7ddb08dd001e','liubei@163.com',NULL,NULL,'刘备',NULL,NULL,'b837d9b9d0b6e2c3a63892d1893103d5','18077777771','',NULL,'21ec3c','0','0','admin','2020-02-15 19:34:34','刘备'),('40283e8170489029017048a2c9610022',NULL,'admin','2020-02-15 19:36:33','40283e816f7dcf4f016f7dde4c710021','guanyu@163.com',NULL,'127.0.0.1','关羽','2020-02-15 19:49:12',NULL,'7e022364f4c638a8556fce09e347b9f3','18077777772','',NULL,'cc41c3','0','0','admin','2020-02-15 19:36:41','关羽'),('40283e8170489029017048a4533b0027',NULL,'admin','2020-02-15 19:38:14','40283e816f92646d016f926c38e4000f','sunjian@163.com',NULL,NULL,'孙坚',NULL,NULL,'2ee6a6389daec9c00d94e86801048c99','18099999991','',NULL,'8a2f6b','0','0','admin','2020-02-15 19:46:41','孙坚'),('40283e8170489029017048a4f302002a',NULL,'admin','2020-02-15 19:38:55','40283e816f92646d016f926c38e4000f','sunquan@163.com',NULL,NULL,'孙权',NULL,NULL,'776e22032bcf0eac0496ab9747e6a2a4','18099999992','',NULL,'1a3cd9','0','0','admin','2020-02-15 19:46:45','孙权'),('40283e8170489029017048a63954002d',NULL,'admin','2020-02-15 19:40:18','40283e816f92646d016f926c38e4000f','zhouyu@163.com',NULL,NULL,'周瑜',NULL,NULL,'3c430eeb3c2d93eb3fdc2b3b4c705408','18099999993','',NULL,'4f0bb3','0','0','admin','2020-02-15 19:46:47','周瑜'),('402855816f1e2a2e016f1e2a45af0001',NULL,'system','2019-12-19 20:38:04','402855816f35ac13016f35cc740b0017','daffodil@admin.com','Y','127.0.0.1','admin','2020-02-16 13:42:43',NULL,'ae4bac50de6e62586a336465bb83cdb1','18088888888','超级管理员',NULL,'5b3f96','0','0','admin','2020-02-15 19:18:26','达佛');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_cn_model_type`
--

DROP TABLE IF EXISTS `act_cn_model_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_cn_model_type` (
  `model_type_id` varchar(255) NOT NULL,
  `ancestors` varchar(255) DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type_name` varchar(30) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`model_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_cn_model_type`
--

LOCK TABLES `act_cn_model_type` WRITE;
/*!40000 ALTER TABLE `act_cn_model_type` DISABLE KEYS */;
INSERT INTO `act_cn_model_type` VALUES ('40283e81704c84ee01704ca089f00003','root','admin','2020-02-16 14:12:34',1,'root','','0','财务报销',NULL,NULL),('40283e81704c84ee01704ca0c5150005','root','admin','2020-02-16 14:12:50',2,'root','','0','出勤考勤',NULL,NULL),('40283e81704c84ee01704ca0ff4e0007','root,40283e81704c84ee01704ca089f00003','admin','2020-02-16 14:13:04',1,'40283e81704c84ee01704ca089f00003','','0','员工报销',NULL,NULL),('40283e81704c84ee01704ca121d90009','root,40283e81704c84ee01704ca089f00003','admin','2020-02-16 14:13:13',2,'40283e81704c84ee01704ca089f00003','','0','经理报销',NULL,NULL);
/*!40000 ALTER TABLE `act_cn_model_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_shiro_filter`
--

DROP TABLE IF EXISTS `sys_shiro_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_shiro_filter` (
  `filter_id` varchar(255) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `filter_key` varchar(255) DEFAULT NULL,
  `filter_name` varchar(255) DEFAULT NULL,
  `filter_value` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `order_num` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_shiro_filter`
--

LOCK TABLES `sys_shiro_filter` WRITE;
/*!40000 ALTER TABLE `sys_shiro_filter` DISABLE KEYS */;
INSERT INTO `sys_shiro_filter` VALUES ('40283e8170470c0401704712b9b7000e','admin','2020-02-15 12:19:34','/favicon.ico**','网页图标','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:36:31',11),('40283e8170470c04017047131fbb0010','admin','2020-02-15 12:20:01','/css/**','样式库','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:36:43',10),('40283e8170470c04017047139e870012','admin','2020-02-15 12:20:33','/fonts/**','字体库','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:36:21',9),('40283e8170470c0401704713db7f0014','admin','2020-02-15 12:20:49','/img/**','图片库','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:34:22',8),('40283e8170470c0401704714d7440016','admin','2020-02-15 12:21:53','/ajax/**','插件库','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:36:17',7),('40283e8170470c040170471520a60018','admin','2020-02-15 12:22:12','/js/**','脚本库','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:34:18',6),('40283e8170470c04017047157601001a','admin','2020-02-15 12:22:34','/kaptcha/**','验证码','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:34:14',5),('40283e8170470c040170471762a2001c','admin','2020-02-15 12:24:40','/modeler/**','流程设计器','anon','对静态资源设置匿名访问','0','admin','2020-02-15 12:34:09',4),('40283e8170470c0401704717cae8001e','admin','2020-02-15 12:25:07','/logout','退出请求','logout','退出请求过滤限制访问','0','admin','2020-02-15 12:37:03',3),('40283e8170470c040170471832660020','admin','2020-02-15 12:25:33','/login','登录请求','anon,kaptchaValidate','登录请求过滤限制访问','0','admin','2020-02-15 12:37:15',2),('40283e8170470c040170471892750022','admin','2020-02-15 12:25:58','/**','所有请求','user,kickout,onlineSession','所有请求过滤限制访问','0','admin','2020-02-15 12:38:05',1);
/*!40000 ALTER TABLE `sys_shiro_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_data` (
  `dict_data_id` varchar(255) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `css_class` varchar(100) DEFAULT NULL,
  `dict_code` varchar(255) DEFAULT NULL,
  `dict_label` varchar(100) DEFAULT NULL,
  `dict_sort` bigint(20) DEFAULT NULL,
  `dict_type` varchar(100) DEFAULT NULL,
  `dict_value` varchar(100) DEFAULT NULL,
  `is_default` varchar(255) DEFAULT NULL,
  `list_class` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`dict_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` VALUES ('297eb3bf6f41395e016f413e74160005','admin','2019-12-26 16:06:50','',NULL,'目录',1,'sys_menu_type','catalog','Y','primary','菜单类型目录','0','admin','2020-01-07 11:10:01'),('297eb3bf6f41395e016f413f56230008','admin','2019-12-26 16:07:47','',NULL,'菜单',2,'sys_menu_type','menu','N','success','菜单类型菜单','0','admin','2020-01-07 11:10:11'),('297eb3bf6f41395e016f413fda7a000a','admin','2019-12-26 16:08:21','',NULL,'按钮',3,'sys_menu_type','button','N','warning','菜单类型按钮','0','admin','2020-01-07 11:10:34'),('40283e816fad93ca016fada0f91c000e','admin','2020-01-16 17:13:26','',NULL,'在办',1,'sys_flow_status','0','Y','info','','0','admin','2020-01-16 17:16:13'),('40283e816fad93ca016fada159960010','admin','2020-01-16 17:13:50','',NULL,'已办',2,'sys_flow_status','1','N','success','','0','admin','2020-01-18 11:39:33'),('40283e816fad93ca016fada1a1a60012','admin','2020-01-16 17:14:09','',NULL,'删除',3,'sys_flow_status','2','N','danger','','0',NULL,NULL),('402855816f31c4de016f31c4f5030009','system','2019-12-23 15:59:49',NULL,NULL,'男',1,'sys_user_sex','0','Y','info','性别男','0',NULL,NULL),('402855816f31c4de016f31c4f503000a','system','2019-12-23 15:59:49',NULL,NULL,'女',2,'sys_user_sex','1','N','info','性别女','0',NULL,NULL),('402855816f31c4de016f31c4f519000b','system','2019-12-23 15:59:49',NULL,NULL,'未知',3,'sys_user_sex','2','N','info','性别未知','0',NULL,NULL),('402855816f31c4de016f31c4f521000c','system','2019-12-23 15:59:49',NULL,NULL,'显示',1,'sys_show_hide','0','Y','primary','显示菜单','0',NULL,NULL),('402855816f31c4de016f31c4f529000d','system','2019-12-23 15:59:49',NULL,NULL,'隐藏',2,'sys_show_hide','1','N','danger','隐藏菜单','0',NULL,NULL),('402855816f31c4de016f31c4f529000e','system','2019-12-23 15:59:49',NULL,NULL,'正常',1,'sys_data_status','0','Y','primary','正常状态','0',NULL,NULL),('402855816f31c4de016f31c4f538000f','system','2019-12-23 15:59:49',NULL,NULL,'停用',2,'sys_data_status','1','N','warning','停用状态','0',NULL,NULL),('402855816f31c4de016f31c4f54a0010','system','2019-12-23 15:59:49','',NULL,'删除',3,'sys_data_status','2','N','danger','删除状态','1','admin','2020-01-06 17:02:49'),('402855816f31c4de016f31c4f54a0011','system','2019-12-23 15:59:49',NULL,NULL,'是',1,'sys_yes_no','Y','Y','primary','系统默认是','0',NULL,NULL),('402855816f31c4de016f31c4f5590012','system','2019-12-23 15:59:49',NULL,NULL,'否',2,'sys_yes_no','N','N','danger','系统默认否','0',NULL,NULL),('402855816f31c4de016f31c4f5690013','system','2019-12-23 15:59:49',NULL,NULL,'通知',1,'sys_notice_type','1','Y','warning','通知类型','0',NULL,NULL),('402855816f31c4de016f31c4f5690014','system','2019-12-23 15:59:49',NULL,NULL,'公告',2,'sys_notice_type','2','N','success','公告类型','0',NULL,NULL),('402855816f31c4de016f31c4f57e0015','system','2019-12-23 15:59:49',NULL,NULL,'正常',1,'sys_notice_status','0','Y','primary','正常状态','0',NULL,NULL),('402855816f31c4de016f31c4f57e0016','system','2019-12-23 15:59:49',NULL,NULL,'关闭',2,'sys_notice_status','1','N','danger','关闭状态','0',NULL,NULL),('402855816f31c4de016f31c4f58d0017','system','2019-12-23 15:59:49',NULL,NULL,'新增',1,'sys_oper_type','1','N','info','新增操作','0',NULL,NULL),('402855816f31c4de016f31c4f59f0018','system','2019-12-23 15:59:49',NULL,NULL,'修改',2,'sys_oper_type','2','N','info','修改操作','0',NULL,NULL),('402855816f31c4de016f31c4f5a30019','system','2019-12-23 15:59:49',NULL,NULL,'删除',3,'sys_oper_type','3','N','danger','删除操作','0',NULL,NULL),('402855816f31c4de016f31c4f5a3001a','system','2019-12-23 15:59:49',NULL,NULL,'授权',4,'sys_oper_type','4','N','primary','授权操作','0',NULL,NULL),('402855816f31c4de016f31c4f5b2001b','system','2019-12-23 15:59:49',NULL,NULL,'导出',5,'sys_oper_type','5','N','warning','导出操作','0',NULL,NULL),('402855816f31c4de016f31c4f5b2001c','system','2019-12-23 15:59:49',NULL,NULL,'导入',6,'sys_oper_type','6','N','warning','导入操作','0',NULL,NULL),('402855816f31c4de016f31c4f5c4001d','system','2019-12-23 15:59:49',NULL,NULL,'强退',7,'sys_oper_type','7','N','danger','强退操作','0',NULL,NULL),('402855816f31c4de016f31c4f5c4001e','system','2019-12-23 15:59:49',NULL,NULL,'生成',8,'sys_oper_type','8','N','warning','生成操作','0',NULL,NULL),('402855816f31c4de016f31c4f5d4001f','system','2019-12-23 15:59:49',NULL,NULL,'清空',9,'sys_oper_type','9','N','danger','清空操作','0',NULL,NULL),('402855816f31c4de016f31c4f5e30020','system','2019-12-23 15:59:49',NULL,NULL,'成功',1,'sys_common_status','0','N','primary','成功状态','0',NULL,NULL),('402855816f31c4de016f31c4f5e50021','system','2019-12-23 15:59:49',NULL,NULL,'失败',2,'sys_common_status','1','N','danger','失败状态','0',NULL,NULL);
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-16 14:14:09
