package com.daffodil.flowable.service;

import java.io.InputStream;

/**
 * 流程图
 * @author yweijian
 * @date 2020年1月20日
 * @version 1.0
 */
public interface IFlowableDiagramService {

	public InputStream getFlowableDiagramByBusinessKey(String businessKey);
}
